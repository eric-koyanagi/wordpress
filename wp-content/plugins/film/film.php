<?php
/*
Plugin Name: Custom Film
Description: Adds a film post type. This could also be placed in MU plugins so that the client does not need to activate it.
Text Domain: custom-film
*/

// Add new film taxonomies by expanding this array
function get_taxonomy_array() {   
   return array(
      'Genres' => 'genre',
      'Countries' => 'country',
      'Years' => 'years',
      'Actors' => 'actor'
    );
}

// creates all film taxonomies
function create_film_taxonomies() {
  $film_taxonomies = get_taxonomy_array();
  foreach($film_taxonomies as $label => $taxonomy) {
    register_taxonomy($taxonomy, null, array(
          'label' => $label 
       )
    );
  }
}

// registers a new type for films and associates films with our custom taxonomy
function create_film_post_type() {  
  register_post_type( 'custom_film',
    array(
      'labels' => array(
        'name' => ( 'Films' ),
        'singular_name' => ( 'Film' )
      ),
      'public' => true,
      'has_archive' => true,
      'taxonomies' => get_taxonomy_array(),
      'rewrite' => array('slug' => 'films'),
    )
  );
        
}

// helper to easily array_map taxonomy objects to plain taxo names
function map_genres_to_name($array) {
    return ucfirst($array->name);
}

// A simple abstraction since archives and post display the same snippet with extended properties
function get_film_data_html($post) {
    return '<div class="film-information">
        <div><strong>Countries:</strong> ' . implode(', ', array_map(map_genres_to_name, get_the_terms($post->ID, 'country'))) . '</div>
        <div><strong>Genres:</strong> '. implode(', ', array_map(map_genres_to_name, get_the_terms($post->ID, 'genre'))) . '</div>
        <div><strong>Released On:</strong> ' . date("m/d/y", strtotime($post->release_date)) . '</div>
        <div><strong>Ticket Price:</strong> $' . money_format('%i', $post->ticket_price) . '</div>
    </div>';    
}

// adds HTML to the archive page using a content filter hook
function add_film_archive_data_filter( $content ) {
    global $post;
    if ( is_archive() && in_the_loop() && is_main_query() ) {	    
        return $content . get_film_data_html( $post );
    }
     
    return $content;
}

// a shortcode that displays the most recent x films; the formatting could be removed so that the shortcode is more generic
// [latestfilms num="5"]
function get_latest_films( $atts ) {
	$attributes = shortcode_atts( array(
		'num' => 5		
	), $atts );

    $loop = new WP_Query(array(
        'posts_per_page'    => $attributes['num'],
        'post_type'         => 'custom_film',
       )
    );

    if( !$loop->have_posts() ) {
        return false;
    }
    	    
    echo '<aside class="widget widget_text"><h3 class="widget-title">Recent Films</h3><ul>';
    
    while( $loop->have_posts() ) {
        $loop->the_post();
        echo '<li><a href="' . get_post_permalink($post) . '">' . get_the_title($post->ID) . '</a></li>';
    }
    
    echo '</ul></aside>';
    
    wp_reset_postdata();	
}

add_action('init', 'create_film_taxonomies');
add_action('init', 'create_film_post_type');
add_filter('the_content', 'add_film_archive_data_filter');
add_shortcode( 'latestfilms', 'get_latest_films' );