<?php
function add_styles() {

    // the the parent's stylesheet to this child theme
    $parent_style = 'unite'; 

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    
    // add my child CSS as a dependency of my parent's
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}

add_action( 'wp_enqueue_scripts', 'add_styles' );