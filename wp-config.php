<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'CQ:8P8**bYd|&uv~TWqp@?~}W1W+I@6?cx^/1[fXPC:(_UJs$Eu{KM4%za|g)!j*');
define('SECURE_AUTH_KEY',  'rtyz(UWz}t<?NM^UuBolhU>pp%KA9nGck63*0FQjH7DI^Gwna2Dyhhm<,bl~Gs[+');
define('LOGGED_IN_KEY',    'IBv7Zn3.dVs`+,.rJ7j4SUU,x%rm%4wyAUInj;n5^~q:=KF6J%tzNdw/%K(inh-f');
define('NONCE_KEY',        'EiV_Xf^oldi~!hL;>y=BoP,9DGF1F.ll|iSG?88pn)j2%[ {;2pVXvz:J~cQMwjF');
define('AUTH_SALT',        'n<b/&#?F7dL6lmD/p-Lt(yNA{AQ~8m;3i?YXlyX=x.3.,vctso%nCFYO9auu*npL');
define('SECURE_AUTH_SALT', 'Mut4Emrl~srCs<Co^QyM[E}?%k?7VKfOJY!^VGSKXM%K<OJW/=J*G&~/fyutz=YM');
define('LOGGED_IN_SALT',   'Ym1L}7DBVl1ja>{;;]}%hZGf:6egyEb?LZmzV0tHN6gGd*=|r^[+IReG}VLkjSQ;');
define('NONCE_SALT',       'WXV}N rd?L)6nW{Xkm;P)Q848UK]B;?ELlem?>02`(,B{tX(|_cW1/MQ+ R*0X]O');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
